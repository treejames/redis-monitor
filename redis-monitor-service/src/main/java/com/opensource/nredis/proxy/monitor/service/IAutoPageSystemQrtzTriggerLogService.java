package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.AutoPageSystemQrtzTriggerLog;

/**
* service interface
*
* @author liubing
* @date 2016/12/30 08:41
* @version v1.0.0
*/
public interface IAutoPageSystemQrtzTriggerLogService extends IBaseService<AutoPageSystemQrtzTriggerLog>,IPaginationService<AutoPageSystemQrtzTriggerLog>  {
}

