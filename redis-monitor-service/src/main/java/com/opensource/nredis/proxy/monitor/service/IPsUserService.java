package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.PsUser;

/**
* service interface
*
* @author liubing
* @date 2016/12/12 12:20
* @version v1.0.0
*/
public interface IPsUserService extends IBaseService<PsUser>,IPaginationService<PsUser>  {
}

